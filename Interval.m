classdef Interval
    %Interval Summary of this class goes here
    %   Detailed explanation goes here
    
    % HybridAutomaton/Interval.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    enumeration
        Trajectory(@Interval.trajectory)
        FinalState(@Interval.final_state)
    end
    
    properties
        evaluationIntervalFcn
    end
    
    methods
        function [intvl] = Interval(fcn)
            intvl.evaluationIntervalFcn = fcn;
        end
        
        function [v] = interval(intvl, t_init, t_sim_ub)
            f = intvl.evaluationIntervalFcn;
            v = f(t_init, t_sim_ub);
        end
    end
    
    methods (Static)
        function [invl] = trajectory(t_init, t_sim_ub)
            invl = [t_init, t_sim_ub];
        end
        
        function [invl] = final_state(t_init, t_sim_ub)
            invl = [t_init, (t_init + t_sim_ub)/2, t_sim_ub];
        end
    end
    
end

