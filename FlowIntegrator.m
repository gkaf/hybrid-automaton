classdef (Abstract) FlowIntegrator
    %FlowIntegrator Class containing the flow slover and its parameters.
    %   Using: [ode23s, ode45]
    
    % HybridAutomaton/FlowIntegrator.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties (Abstract, Constant)
        solver
    end
    
    properties
        integrator_options
    end
    
    methods
        function [fi] = FlowIntegrator(rel_tol, abs_tol, max_step)
            % Integration parameters
            % RelTol: scalar, controls the number of correct digits in all solution components, except those smaller than the absolute tolerance AbsTol
            % AbsTol: vector, the threshold below which the value of the solution component becomes unimportant.
            options = odeset('RelTol', rel_tol, 'AbsTol', abs_tol, 'MaxStep', max_step);
            
            fi.integrator_options = options;
        end
        
        %function [dx] = fcn(vsf, t, x, u)
        %    [f,M] = vsf.flowDynamics(u);
        %    dx = M\f(t,x);
        %end
        
        function [ts, zxs, te, zxe, ie, terminal_idx] = integrator(fi, flow, event, interval, t, z, t_sim_ub)
            t_init = t;
            z_init = z;
            
            [event_fcn, idx_terminal_bitmap] = event();
            [f, M] = flow();
            if t_sim_ub > t_init
                % x_0: is provided individually to support maps.
                
                options = odeset(fi.integrator_options, 'Events', event_fcn);
                options = odeset(options, 'Mass', M);
                [ts, zxs, te, zxe, ie] = fi.solver(f, interval(t_init, t_sim_ub), z_init, options);
                ts = transpose(ts);
                zxs = transpose(zxs);
                te = transpose(te);
                zxe = transpose(zxe);
                ie = transpose(ie);
                
                % sol <- {t, x, te, xe, ie}
                %st = StateTranscript(discreteSate, x_init, t, z, y, te, ze, ie);
                
                if isempty(ie)
                    terminal_idx = [];
                else
                    terminal_idx_stack = Stack();
                    for k = 1:length(ie)
                        evt = ie(k);
                        if idx_terminal_bitmap(evt)
                            terminal_idx_stack = terminal_idx_stack.push(k);
                        end
                    end
                    terminal_idx = transpose(cell2mat(terminal_idx_stack.toCell()));
                end
            else
                ts = [];
                zxs = [];
                te = [];
                zxe = [];
                ie = [];
                terminal_idx = {};
            end
        end
        
    end
    
end

