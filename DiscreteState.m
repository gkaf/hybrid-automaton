classdef (Abstract) DiscreteState
    %DiscreteState Interface for the discrete state of the hybrid automaton
    %   The interface for the states of the hybrid automaton. The states
    %   are differentiated in flow and jump states.
    
    % HybridAutomaton/DiscreteState.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        % State variable, constant in each class instance. Contains
        % information that are used in the computation that the hybrid
        % automaton performs.
        discreteStateData
        % Discrete control signal. Using Utkin's convention for modelling
        % discontinuities.
        u
        
        % Discrete state id from an enumeration of the automaton state
        % classes. Each calss can have multiple members, each one with
        % different control input u.
        discreteStateEnumeration
        
        % Memoized parameter for the continuum out put of the system. The
        % parameter is set by the state generator to ensure it is the same
        % for all states of an automaton. Allows the prealocation of
        % storage for the output.
        continuum_output_size
    end
    
    methods (Abstract)
        [stateTransition, t_post, x_post, Dt_ub] = stateTranscript(ds, t_init, x_init, Dt_state_ub, Dt_ub);
    end
    
    methods (Abstract)
        [y] = continuumOutput(ds, t, x); % t,x -> R^n, also used to code the switching function, e.t.c.
        [zy] = compressContinuumOutput(ds, y);
        [n] = compressedContinuumOutputSize(ds); % n, the dimension of the momiozed output vector zy
    end
    
    methods (Abstract)
        [outputHandler] = getOutputHandler(ds);
    end
    
    methods
        function [ds] = DiscreteState(discreteStateEnumeration, discreteStateData, u, continuum_output_size)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            ds.discreteStateEnumeration = discreteStateEnumeration;
            ds.discreteStateData = discreteStateData;
            ds.u = u;
            ds.continuum_output_size = continuum_output_size;
        end
        
        function [n] = continuumOutputSize(ds)
            % n, the dimension of the output vector y
            n = ds.continuum_output_size;
        end
        
        function [u] = discreteOutput(ds)
            %discreteOutput Exctract the discrete control signal
            %   This method extract the discrete control signal of the
            %   class. The control signal 'u' acts as an identifier for the
            %   class instance.
            u = ds.u;
        end
    end
end

