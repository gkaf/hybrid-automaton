classdef (Abstract) DiscreteStateFlow < DiscreteState
    %DiscreteState Interface for a flow state
    %   Detailed explanation goes here
    
    % HybridAutomaton/DiscreteState.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        % Continuum solver
        flowIntegrator
        evaluationInterval
    end
    
    methods (Abstract)
        % The initial time and continuum state can influence the flow
        % function. This is not a feature in variable structure systems,
        % but it accurs in more generic hybrid automata.
        [f, M] = flow(ds, t, x); % flow: <discrete state> x <initial state> x <initial time> -> <first order derivative> x <mass matrix>
        [e, idx_terminal_bitmap] = event(ds, t, x); % event: <time> x <mapped continuum state> -> <ode event function>, [te, zxe, ie] = e(t, zx)
        [stateId] = transition(ds, flowTranscript);
        
        [zx] = map(ds, x); % Projection onto the flow space
        [holographicProjection] = getHolographicProjection(ds);
    end
    
    methods
        function [ds] = DiscreteStateFlow(discreteStateEnumeration, discreteStateData, u, ...
                flowIntegrator, interval, continuum_output_size)
            %DiscreteStateFlow Construct an instance of this class
            %   Detailed explanation goes here
            ds@DiscreteState(discreteStateEnumeration, discreteStateData, u, continuum_output_size);
            
            ds.flowIntegrator = flowIntegrator;
            ds.evaluationInterval = interval;
        end
        
        function [phi] = flowConstructor(ds, t_sim_ub)
            phi = @flow_integrator;
            
            function [flowTranscript] = flow_integrator(t_init, x_init)
                z_init = ds.map(x_init);
                
                [ts, zxs, te, zxe, ie, terminal_idx] = ds.flowIntegrator.integrator(@flow, @event, @interval, t_init, z_init, t_sim_ub);
                
                if ~isempty(ts)
                    % Keep only the points after the initial one; initial point
                    % is the same as the previous state's last point.
                    % That is the transition is in the interval (t_0, t_1].
                    ts = ts(2:end);
                    zxs = zxs(:, 2:end);
                    
                    holographicProjection = ds.getHolographicProjection();
                    flowTranscript = FlowTranscript(t_init, x_init, ...
                        ts, zxs, ...
                        te, zxe, ie, terminal_idx, ...
                        holographicProjection);
                else
                    flowTranscript = {};
                end
                
                function [event, idx_terminal_bitmap] = event()
                    [event, idx_terminal_bitmap] = ds.event(t_init, x_init);
                end
                
                function [f, M] = flow()
                    [f, M] = ds.flow(t_init, x_init);
                end
            end
            
            function [invl] = interval(t_init, t_sim_ub)
                invl = ds.evaluationInterval.interval(t_init, t_sim_ub);
            end
        end
        
        function [stateTransition, t_post, x_post, Dt_ub] = stateTranscript(ds, t_init, x_init, Dt_state_ub, Dt_ub)
            t_state_sim_ub = t_init + Dt_state_ub;
            t_sim_ub = t_init + Dt_ub;
            t_ub = min(t_state_sim_ub, t_sim_ub);
            
            phi = ds.flowConstructor(t_ub);
            flowTranscript = phi(t_init, x_init);
            
            if ~isempty(flowTranscript)
                stateId = ds.transition(flowTranscript);
                stateTransition = stateId;
                
                t_post = flowTranscript.finalTimeInstance();
                x_post = flowTranscript.finalState();
            else
                stateTransition = {};
                t_post = t_init;
                x_post = x_init;
            end
            
            Dt = t_post - t_init;
            Dt_ub = Dt_ub - Dt;
        end
    end
end

