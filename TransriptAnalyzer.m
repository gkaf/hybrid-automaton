classdef (Abstract) TransriptAnalyzer
    %TransriptAnalyzer Summary of this class goes here
    %   Detailed explanation goes here
    
    % HybridAutomaton/TransriptAnalyzer.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        transcript
    end
    
    methods (Abstract)
        % Sampling allows easier plotting, but doen not have anuy impact
        % in the size of the output object. The size of the output should
        % be constrolled in the experiment level, by running consecutive
        % experiments, and storing the results individually. Post
        % processing should allow for combination of the results.
        [ta, t, x] = filter_trajectory(ta, t, x);
        [ta, t, y] = filter_output(ta, t, y);
        [ta, t, y, u] = filter_control(ta, t, x, u);
        [ta, t, x, y, u] = filter_signals(ta, t, x, u, y);
    end
    
    methods
        function [ta] = TransriptAnalyzer(transcript)
            ta.transcript = transcript;
        end
        
        function [t, x] = trajectory(ta)
            ht = ta.transcript;
            tq = Queue();
            xq = Queue();
            while ~ht.isEmpty()
                eraTranscript = ht.currentEraTranscript();
                ts = eraTranscript.timeInstances();
                xs = eraTranscript.trajectory();
                
                [ta, ts, xs] = ta.filter_trajectory(ts, xs);
                
                tq = tq.enqueue(ts);
                xq = xq.enqueue(xs);
                ht = ht.previousEra();
            end
            t = tq.toCell();
            x = xq.toCell();
        end
        
        function [t, y] = output(ta)
            ht = ta.transcript;
            tq = Queue();
            yq = Queue();
            while ~ht.isEmpty()
                eraTranscript = ht.currentEraTranscript();
                ts = eraTranscript.timeInstances();
                ys = eraTranscript.continuumOutputSignal();
                
                [ta, ts, ys] = ta.filter_output(ts, ys);
                
                tq = tq.enqueue(ts);
                yq = yq.enqueue(ys);
                ht = ht.previousEra();
            end
            t = cell2mat(tq.toCell());
            y = cell2mat(yq.toCell());
        end
        
        function [t, y, u] = control(ta)
            ht = ta.transcript;
            tq = Queue();
            yq = Queue();
            uq = Queue();
            while ~ht.isEmpty()
                eraTranscript = ht.currentEraTranscript();
                ts = eraTranscript.timeInstances();
                ys = eraTranscript.continuumOutputSignal();
                us = eraTranscript.discreteOutputSignal();
                
                [ta, ts, ys, us] = ta.filter_control(ts, ys, us);
                
                tq = tq.enqueue(ts);
                yq = yq.enqueue(ys);
                uq = uq.enqueue(us);
                ht = ht.previousEra();
            end
            t = cell2mat(tq.toCell());
            y = cell2mat(yq.toCell());
            u = cell2mat(uq.toCell());
        end
        
        function [t, x, y, u] = signals(ta)
            ht = ta.transcript;
            tq = Queue();
            xq = Queue();
            uq = Queue();
            yq = Queue();
            while ~ht.isEmpty()
                eraTranscript = ht.currentEraTranscript();
                ts = eraTranscript.timeInstances();
                [xs, ys] = eraTranscript.continuumSignals();
                us = eraTranscript.discreteOutputSignal();
                
                [ta, ts, xs, us, ys] = ta.filter_signals(ts, xs, us, ys);
                
                tq = tq.enqueue(ts);
                xq = xq.enqueue(xs);
                uq = uq.enqueue(us);
                yq = yq.enqueue(ys);
                ht = ht.previousEra();
            end
            t = tq.toCell();
            x = xq.toCell();
            u = uq.toCell();
            y = yq.toCell();
        end
        
        function [te, xe, ie] = transcript_events(ta)
            ht = ta.transcript;
            teq = Queue();
            xeq = Queue();
            ieq = Queue();
            while ~ht.isEmpty()
                eraTranscript = ht.currentEraTranscript();
                
                [era_te, era_xe, era_ie] = eraTranscript.eraEvents();
                
                teq = teq.enqueue(era_te);
                xeq = xeq.enqueue(era_xe);
                ieq = ieq.enqueue(era_ie);
                ht = ht.previousEra();
            end
            te = cell2mat(teq.toCell());
            xe = cell2mat(xeq.toCell());
            ie = cell2mat(ieq.toCell());
        end
    end
    
end

