classdef FlowTranscript
    %FlowTranscript Class storing the output of en integration by the 'flow
    %integrator'.
    %   The states in our case are visited for time intervals of the form
    %   [t_s, t_e). In the duration of the time interval the state is
    %   constant. Note that t_e >= t_s.
    
    % HybridAutomaton/FlowEraTranscript.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        % Continuous trajectory data
        Ts % Continuous transition time interval
        ZXs % Continuous transition trajectory
        % Data required to for the map invertion (hologram)
        t_init
        x_init
        
        % Events during continuous transition
        te
        zxe
        ie
        % Terminal events
        terminal_idx % Indicies of events that ends the discrete state era
        
        holographicProjection
    end
    
    methods
        function [ft] = FlowTranscript(t_init, x_init, Ts, ZXs, te, zxe, ie, terminal_idx, holographicProjection)
            % Continuous transition
            if length(Ts) < 1
                msgID = 'SolverError:SingleElementOutput';
                msgtext = 'Error occur in the integrator. Solution was not continued and contains single element.';
                ME = MException(msgID, msgtext);
                throw(ME);
            end
            
            % Trajectory data
            ft.Ts = Ts;
            ft.ZXs = ZXs;
            
            % Holographic inversion data
            ft.t_init = t_init;
            ft.x_init = x_init;
            
            % Events during continuous transition
            ft.te = te;
            ft.zxe = zxe;
            ft.ie = ie;
            
            ft.terminal_idx = terminal_idx;
            
            ft.holographicProjection = holographicProjection;
        end
        
        function [t_fin] = finalTimeInstance(ft)
            t_fin = ft.Ts(end);
        end
        
        function [x_fin] = finalState(ft)
            zx_fin = ft.ZXs(:,end);
            x_fin = ft.holographicProjection.hologram(ft.x_init, zx_fin);
        end
        
        function [t_init] = initialTimeInstance(ft)
            t_init = ft.t_init;
        end
        
        function [x_init] = initialState(ft)
            x_init = ft.x_init;
        end
        
        function [ei] = e_idx(ft)
            ei = ft.ie(ft.terminal_idx);
        end
        
        function [x] = trajectory(ft)
            dimensions = length(ft.x_init);
            N = length(ft.Ts);
            x = zeros(dimensions, N+1);
            for n = 1:N
                zx_n = ft.ZXs(:,n);
                x_shift_n = ft.holographicProjection.hologram(ft.x_init, zx_n);
                x(:,n+1) = x_shift_n;
            end
            x(:,1) = ft.x_init;
        end
        
        function [t] = time(ft)
            N = length(ft.Ts);
            t = zeros(1,N+1);
            for n = 1:N
                t(n+1) = ft.Ts(n);
            end
            t(1) = ft.t_init;
        end
        
        function [zx] = storedTrajectory(ft)
            zx = ft.ZXs;
        end
        
        function [ts] = storedTime(ft)
            ts = ft.Ts;
        end
        
        function [te, zxe, ie] = eventsProj(ft)
            te = ft.te;
            zxe = ft.zxe;
            ie = ft.ie;
        end
    end
    
    methods            
        function [t, x, e] = events(ft)
            t = ft.te;
            e = ft.ie;
            
            dimensions_x = length(ft.x_init);
            N = length(ft.ie);
            x = zeros(dimensions_x, N);
            for n = 1:N
                zxe_n = ft.zxe(:,n);
                x(:,n) = ft.holographicProjection.hologram(ft.x_init, zxe_n);
            end
        end
        
        function [t, z, e] = terminalEventProj(ft, n)
            idx = ft.terminal_idx(n);
            
            t = ft.te(idx);
            e = ft.ie(idx);
            z = ft.zxe(:,idx);
        end
        
        function [te, zxe, ie] = terminalEventsProj(ft)
            te = ft.te(ft.terminal_idx);
            ie = ft.ie(ft.terminal_idx);
            zxe = ft.zxe(ft.terminal_idx);
        end
        
        function [x] = hologram(ft, z)
            x = ft.holographicProjection.hologram(ft.x_init, z);
        end
        
        function [t, x, e] = terminalEventTrajectory(ft, n)
            idx = ft.terminal_idx(n);
            
            t = ft.te(idx);
            e = ft.ie(idx);
            z = ft.zxe(:,idx);
            
            x = ft.holographicProjection.hologram(ft.x_init, z);
        end
        
        function [te, xe, ie] = terminalEventsTrajectory(ft)
            te = ft.te(ft.terminal_idx);
            ie = ft.ie(ft.terminal_idx);
            
            dimensions_x = length(ft.x_init);
            N = length(ft.terminal_idx);
            xe = zeros(dimensions_x, N);
            for n = 1:N
                idx = ft.terminal_idx(n);
                zxe_n = ft.zxe(:,idx);
                xe(:,n) = ft.holographicProjection.hologram(ft.x_init, zxe_n);
            end
        end
    end
    
end

