classdef HybridAutomaton
    %HybridAutomaton Summary of this class goes here
    %   The discrete state will be represented by u. The automaton operates
    %   in the following cycle:
    %       1. u,x -> u,x
    %       2. u,x -> u,e
    %       3. u,e -> u
    
    % HybridAutomaton/HybridAutomaton.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        % Discrete state objects (single instance per state)
        discrete_state_generator
    end
    
    methods
        function [ha] = HybridAutomaton(discreteStateGenerator)
            %HybridAutomaton Construct an instance of this class
            %   Detailed explanation goes here
            ha.discrete_state_generator = discreteStateGenerator;
        end
        
        function [discreteState] = getState(ha, stateId)
            discreteState = ha.discrete_state_generator.generateState(stateId);
        end
        
        function [computationOutput, Dt_ub] = evaluate(ha, stateTransition, t, x, Dt_state_ub, Dt_ub)
            while ~stateTransition.isTerminal() && Dt_ub > 0
                stateId = stateTransition;
                
                discrete_state = ha.getState(stateId);
                [stateTransition, t, x, Dt_ub] = discrete_state.stateTranscript(t, x, Dt_state_ub, Dt_ub);
            end
            
            if stateTransition.isTerminal()
                stateTerminal = stateTransition;
                computationOutput = stateTerminal.computationOutput();
            else
                if isempty(stateTransition)
                    msgID = 'AutomatonEvaluationError:EmptyStateTransition';
                    msgtext = 'Error occur in the evaluation of the hybrid automaton. Return state is empty. Possibly unknown index in state transition.';
                else
                    msgID = 'AutomatonEvaluationError:UnknownError';
                    msgtext = 'An unknown error occur in the evaluation of the hybrid automaton.';
                end
                ME = MException(msgID, msgtext);
                throw(ME);
            end
        end
    end
end

