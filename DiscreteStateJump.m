classdef (Abstract) DiscreteStateJump < DiscreteState
    %DiscreteStateJump SInterface for a jump state
    %   Detailed explanation goes here
    
    % HybridAutomaton/DiscreteState.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
    end
    
    methods (Abstract)
        [e_idx] = guard(ds, t, x);
        [stateTransition, t_post, x_post] = jump(ds, e_idx, t_pre, x_pre);
    end
    
    methods
        function [ds] = DiscreteStateJump(discreteStateEnumeration, discreteStateData, u, continuum_output_size)
            %DiscreteStateJump Construct an instance of this class
            %   Detailed explanation goes here
            ds@DiscreteState(discreteStateEnumeration, discreteStateData, u, continuum_output_size);
        end
        
        function [stateTransition, t_post, x_post, Dt_ub] = stateTranscript(ds, t_init, x_init, Dt_state_ub, Dt_ub)
            e_idx = ds.guard(t_init, x_init);
            if Dt_ub < 0 || Dt_state_ub < 0
                stateTransition = StateTerminal(ds.discreteStateData);
                
                t_post = t_init;
                x_post = x_init;
            else
                [stateTransition, t_post, x_post] = ds.jump(e_idx, t_init, x_init);
            end
        end
    end
end

