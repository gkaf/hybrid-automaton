classdef StateId < StateTransition
    %StateId Summary of this class goes here
    %   Detailed explanation goes here
    
    % HybridAutomaton/StateId.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        discreteStateData
        discreteStateEnumeration
        u
    end
    
    methods
        function [si] = StateId(discreteStateEnumeration, discreteStateData, u)
            si@StateTransition();
            si.discreteStateEnumeration = discreteStateEnumeration;
            si.discreteStateData = discreteStateData;
            si.u = u;
        end
        
        function [u] = discreteOutput(si)
            u = si.u;
        end
        
        function [ind] = eq(stateId, stateId_target)
            ind_type = stateId.discreteStateEnumeration == stateId_target.discreteStateEnumeration;
            ind_discrete_ouput = stateId.discreteOutput() == stateId_target.discreteOutput();
            
            ind = ind_type && ind_discrete_ouput;
        end
    end
    
    methods (Static)
        function [ind] = isTerminal()
            ind = false;
        end
    end
    
end

