classdef (Abstract) HolographicProjection
    %HolographicProjection Summary of this class goes here
    %   Detailed explanation goes here
    
    % HybridAutomaton/OutputHandler.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
    end
    
    
    methods (Abstract)
        [x] = hologram(hp, x_init, zx); % Partial inverse of the projection to the state space
    end
    
    methods
        function [hp] = HolographicProjection()
        end
    end
end

