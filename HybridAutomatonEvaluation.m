classdef HybridAutomatonEvaluation
    %HybridAutomatonEvaluation Summary of this class goes here
    %   Detailed explanation goes here
    
    % HybridAutomaton/HybridAutomatonEvaluation.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        hybridAutomaton
        
        % Evaluation Parameters
        Dt_state_ub
        Dt_ub
        
        % Discrete state transition
        stateId
        % Continuous state
        x
        t
    end
    
    methods
        function [hae] = HybridAutomatonEvaluation(hybridAutomaton, ...
                Dt_state_ub, Dt_ub, ...
                stateId, t, x)
            hae.hybridAutomaton = hybridAutomaton;
            
            hae.Dt_state_ub = Dt_state_ub;
            hae.Dt_ub = Dt_ub;
            
            hae.stateId = stateId;
            hae.t = t;
            hae.x = x;
        end
        
        function [computationOutput, Dt] = evaluate(hae)
            ha = hae.hybridAutomaton;
            [computationOutput, Dt_ub_rem] = ha.evaluate(hae.stateId, hae.t, hae.x, hae.Dt_state_ub, hae.Dt_ub);
            Dt = hae.Dt_ub - Dt_ub_rem;
        end
    end
    
end

