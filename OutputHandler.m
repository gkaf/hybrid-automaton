classdef (Abstract) OutputHandler
    %OutputHandler Summary of this class goes here
    %   Detailed explanation goes here
    
    % HybridAutomaton/OutputHandler.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
    end
    
    methods
        function [oh] = OutputHandler()
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
        end
    end
    
    methods (Abstract)
        [y] = reconstructContinuumOutput(oh, t, x, zy);  % <compresed data>,t,x -> R^n, also used to code the switching function, e.t.c.
    end
end

