classdef (Abstract) DiscreteStateEnumeration
    %DiscreteStateEnumeration Enumeration of the automaton states
    %   Each automaton provides an enumeratio of its states.
    
    % HybridAutomaton/DiscreteStateEnumeration.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    methods
        function [dse] = DiscreteStateEnumeration()
        end
    end
    
end

