classdef (Abstract) DiscreteStateGenerator
    %DiscreteStateGenerator Summary of this class goes here
    %   Detailed explanation goes here
    
    % HybridAutomaton/DiscreteStateGenerator.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        continuum_output_size
    end
    
    methods (Abstract)
        [discreteState] = synthesizeState(dsg, discreteStateEnumeration, discreteStateData, u, output_size);
    end
    
    methods
        function [dsg] = DiscreteStateGenerator(continuum_output_size)
            dsg.continuum_output_size = continuum_output_size;
        end
        
        function [n] = continuumOutputSize(dsg)
            n = dsg.continuum_output_size;
        end
        
        function [discreteState] = generateState(dsg, stateId)
            discreteState = dsg.synthesizeState(stateId.discreteStateEnumeration, ...
                stateId.discreteStateData, stateId.u, ...
                dsg.continuum_output_size);
        end
    end
end

