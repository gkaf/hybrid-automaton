classdef StateTerminal < StateTransition
    %StateTerminal Summary of this class goes here
    %   Detailed explanation goes here
    
    % HybridAutomaton/StateTerminal.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        discreteStateData
    end
    
    methods
        function [st] = StateTerminal(discreteStateData)
            st@StateTransition();
            
            st.discreteStateData = discreteStateData;
        end
        
        function [r] = computationOutput(st)
            %computationOutput Output the result of the computation.
            %	A function that outputs the results of the computation in
            %	cases where a transcript is not the only requirement.
            
            r = st.discreteStateData;
        end
    end
    
    methods (Static)
        function [ind] = isTerminal()
            ind = true;
        end
    end
end

