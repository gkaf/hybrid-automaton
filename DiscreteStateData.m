classdef (Abstract) DiscreteStateData
    %DiscreteStateData Class is a variable from a numerable set, storing
    %the data of the discrete state.
    %   A discrerte state can store any numerable amount of data; this
    %   allows for large spaces coresponding to an enumeration to be
    %   represented by a single state. Thus, a finite number of states can
    %   represent a hybrid automaton.
    
    % HybridAutomaton/DiscreteStateData.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
    end
    
    methods
        function [dsd] = DiscreteStateData()
            %DiscreteStateData Construct an instance of this class
            %   Detailed explanation goes here
        end
    end
end

