classdef (Abstract) StateTransition
    %StateEnumeration Summary of this class goes here
    %   Detailed explanation goes here
    
    % HybridAutomaton/StateEnumeration.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
    end
    
    methods (Static, Abstract)
        [ind] = isTerminal();
    end
end

